package com.antongarakh;


import com.antongarakh.monte_carlo.IntegralSolver;
import com.antongarakh.util.Functions;


import java.io.IOException;
import java.util.*;
import static java.lang.Math.*;

import java.util.function.DoubleSupplier;
import java.util.function.DoubleUnaryOperator;

public class Main {
    private static final double ACCURACY = 0.001;
    private static final double FIRST_INTEGRAL_VALUE = 5.95311;
    private static final double SECOND_INTEGRAL_VALUE = 2.4184;

    public static void main(String[] args) throws IOException {
        //method monte carlo
        //1-ый интеграл
        DoubleUnaryOperator function = Functions::function1Density;
        //умножаем БСВ на PI, чтобы получить СВ распределенную на отрезке 0 PI. МО такой СВ = intergral ot 0 PI x * plotnost (1/Pi)
        DoubleSupplier generator = ()-> new Random().nextDouble()* PI;
        System.out.println("Интеграл 1. Пункт 1. " +IntegralSolver.solveIntegral(generator,function,1000));
        int firstIntegralRandomValuesCount = IntegralSolver.findRandomValuesCount(FIRST_INTEGRAL_VALUE,
                ACCURACY,generator,function,1000);
        System.out.println("Интеграл 1. Пункт 2. " +IntegralSolver.solveByQuadrature(1000,0, Math.PI, "1"));
        System.out.println("Интеграл 1. Пункт 3. " +firstIntegralRandomValuesCount);
        System.out.println("Интеграл 1. Пункт 4. " +IntegralSolver.findExpirementsCount(FIRST_INTEGRAL_VALUE,ACCURACY,
                generator,function,firstIntegralRandomValuesCount));
        System.out.println("Интеграл 1. Пункт 5. " +IntegralSolver.findAccuracy(FIRST_INTEGRAL_VALUE,function,firstIntegralRandomValuesCount));
        //2-ой интеграл
        DoubleUnaryOperator function1 = Functions::function2Density;
        generator = ()-> new Random().nextGaussian();
        System.out.println("Интеграл 2. Пункт 1. " +IntegralSolver.solveIntegral(generator,function1,1000));
        int secondIntegralRandomValuesCount = IntegralSolver.findRandomValuesCount(SECOND_INTEGRAL_VALUE,ACCURACY,generator,function1,1000);
        System.out.println("Интеграл 2. Пункт 3. " +secondIntegralRandomValuesCount);
        System.out.println("Интеграл 2. Пункт 4. " +IntegralSolver.findExpirementsCount(SECOND_INTEGRAL_VALUE,ACCURACY,
                generator,function1,secondIntegralRandomValuesCount));
        System.out.println("Интеграл 2. Пункт 5. " + IntegralSolver.findAccuracy(SECOND_INTEGRAL_VALUE,function1,firstIntegralRandomValuesCount));
    }
}
