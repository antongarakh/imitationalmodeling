package com.antongarakh.monte_carlo;

import com.antongarakh.util.Functions;

import java.util.function.DoubleSupplier;
import java.util.function.DoubleUnaryOperator;
import java.util.function.IntFunction;
import java.util.stream.DoubleStream;

public class IntegralSolver {

    public static double solveIntegral(DoubleSupplier randomValuesGenerator, DoubleUnaryOperator randomValueDensity, int randomValuesCount) {
        DoubleStream randomValueStream = DoubleStream.generate(randomValuesGenerator);

        return randomValueStream.limit(randomValuesCount).map(randomValueDensity).average().getAsDouble();
    }

    public static int findRandomValuesCount(double integralValue, double accuracy,
                                            DoubleSupplier randomValuesGenerator,
                                            DoubleUnaryOperator randomValueDensity, int randomValuesCount) {
        double solvedIntegralValue = solveIntegral(randomValuesGenerator, randomValueDensity, randomValuesCount);
        while (Math.abs(solvedIntegralValue - integralValue) > accuracy) {
            randomValuesCount++;
            solvedIntegralValue = solveIntegral(randomValuesGenerator, randomValueDensity, randomValuesCount);
        }
        return randomValuesCount;
    }

    public static int findExpirementsCount(double integralValue, double accuracy,
                                           DoubleSupplier randomValuesGenerator,
                                           DoubleUnaryOperator randomValueDensity, int randomValuesCount) {
        int count = 0;
        for (int i = 0; i < 1000; i++) {
            double solvedIntegralValue = solveIntegral(randomValuesGenerator, randomValueDensity, randomValuesCount);
            if (Math.abs(solvedIntegralValue - integralValue) <= accuracy) {
                count++;
            }
        }
        return count;
    }

    public static double findAccuracy(double integralValue, DoubleUnaryOperator randomValueDensity, int randomValuesCount) {
        IntFunction<DoubleSupplier> generator = (i) -> () -> (double) i / (randomValuesCount + 1);
        double sum = 0;
        for (int i = 0; i < randomValuesCount; i++) {
            DoubleSupplier gen = generator.apply(i);
            sum = sum + solveIntegral(gen, randomValueDensity, randomValuesCount);
        }
        double averageIntegralValue = sum / randomValuesCount;
        return Math.abs(integralValue - averageIntegralValue);
    }

    /**
     * квадратурная формула трапеции
     */
    public static double solveByQuadrature(int n, double a, double b, String functionName) {
        double[] x = new double[n];
        double sum = 0.0;
        for (int i = 0; i < n - 1; i++) {
            x[i] = a + i * (b - a) / n;
            x[i + 1] = a + (i + 1) * (b - a) / n;
            sum = sum + (Functions.function(x[i], functionName) + Functions.function(x[i + 1], functionName))
                    * (x[i + 1] - x[i]) / 2;
        }
        return sum;
    }


}
