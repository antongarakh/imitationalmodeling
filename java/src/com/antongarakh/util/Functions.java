package com.antongarakh.util;

/**
 * Created by Anton on 25/12/2016.
 */
public class Functions {

    private static final String FIRST_FUNCTION = "1";
    private static final String SECOND_FUNCTION = "2";

    public static double function1Density(double x){
        return Math.PI * Math.pow(x * Math.cos(x),2);
    }

    public static double function2Density(double x){
        return Math.sqrt(2*Math.PI)/(Math.pow(x*x+x+1,2)*Math.exp(-x*x/2));
    }

    private static double function1(double x){
        return Math.pow(x * Math.cos(x),2);
    }

    private static double function2(double x){
        return 1/(Math.pow(x*x+x+1,2));
    }

    public static double function(double x, String functionName){
        switch (functionName){
            case FIRST_FUNCTION:
                return function1(x);
            case SECOND_FUNCTION:
                return function2(x);
            default:
                throw new UnsupportedOperationException();
        }
    }
}
